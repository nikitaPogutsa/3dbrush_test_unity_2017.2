﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshGenerator: IMeshGenerator
{
    int[] triangles;
    Vector3[] vertices;
    MeshFilter filter;
    public Transform target;
    public Transform transform;
    //Number of sides
    private int n = 6;
    public delegate Vector3 VectorStuff(Vector3[] vertexInfo);
    private VectorStuff vectorStuff;

    public MeshGenerator(MeshFilter filter, Transform target, Transform transform)
    {
        this.filter = filter;
        this.target = target;
        this.transform = transform;
        vectorStuff = FindYourCenter;
        transform.up = target.position - transform.position;

    }
    //С удалением разберется GC
    public void Draw()
    {
        Append();

    }

    

   private void Append()
    {
        vertices = filter.mesh.vertices;
        triangles = filter.mesh.triangles;
        Vector3[] newVertices = new Vector3[vertices.Length + n];
        for (int i = 0; i < vertices.Length; i++)
        {
            newVertices[i] = vertices[i];
        }
        var center = new Vector3[] { newVertices[newVertices.Length - n - 1], newVertices[newVertices.Length - n - 2], newVertices[newVertices.Length - n - 3], newVertices[newVertices.Length - n - 4], newVertices[newVertices.Length - n - 5], newVertices[newVertices.Length - n - 6] };
        Vector3 faceCenter = (vectorStuff(center));
      
        Vector3 controllerDirection = (transform.InverseTransformPoint(target.position) - faceCenter);
        for (int i = 0; i < n; i++)
        {

            newVertices[newVertices.Length - n + i] = newVertices[newVertices.Length - 2 * n + i]
                + transform.localToWorldMatrix.MultiplyVector(controllerDirection) + Random.insideUnitSphere * 0.02f;
 

        }
        //for (int i = 0; i < n; i++)
        //{

        //    newVertices[newVertices.Length - n + i] = newVertices[newVertices.Length - 2 * n + i]
        //        + transform.localToWorldMatrix.MultiplyVector(controllerDirection);

        //}
       

        filter.sharedMesh.vertices = newVertices;

        //triangles


        int[] newTriangles = new int[triangles.Length + n * 2 * 3];
        for (int i = 0; i < triangles.Length; i++)
        {
            newTriangles[i] = triangles[i];
        }
        //     
        int curTriangle = (newTriangles.Length - 12) / (2 * 3);

        int t0 = curTriangle - n; //6
        int t1 = curTriangle - n + 1; //1
        int t2 = curTriangle - n + 2; //0
        int t3 = curTriangle - n + 3; // 7
        int t4 = curTriangle - n + 4; // 2
        int t5 = curTriangle - n + 5;
        int t6 = (newTriangles.Length - 12) / (2 * 3); //8
        int t7 = (newTriangles.Length - 12) / (2 * 3) + 1;
        int t8 = (newTriangles.Length - 12) / (2 * 3) + 2;
        int t9 = (newTriangles.Length - 12) / (2 * 3) + 3;
        int t10 = (newTriangles.Length - 12) / (2 * 3) + 4;
        int t11 = (newTriangles.Length - 12) / (2 * 3) + 5;
        int t12 = (newTriangles.Length - 12) / (2 * 3) + 6;

        int[] newQuads = new int[] { t0, t1, t6,     t6, t1, t7,
                                     t7, t1, t8,     t8, t1,  t2,
                                     t2,t4, t8,      t8, t4, t10,
                                     t10, t4, t5,    t10, t5, t11,
                                     t11, t5, t9,    t9, t5, t3,
                                     t9,t3,t0,       t0, t6,t9
                                     //t11, t3, t12,   t12

        };

        
        Flip(ref newQuads);

        for (int i = 0; i < newQuads.Length; i++)
        {
            newTriangles[newTriangles.Length - 36 + i] = newQuads[i];
        }
        filter.mesh.triangles = newTriangles;
        filter.mesh.RecalculateNormals();
       
    }

 
    private Vector3 FindYourCenter(Vector3[] face)
    {
        Vector3 sum = new Vector3();
        foreach (var vertex in face)
        {
            sum += vertex;
        }
        return sum / face.Length;
    }

    private void Flip(ref int[] reverse)
    {

        //if (filter != null)
        //{

        //    Vector3[] normals = mesh.normals;
        //    for (int i = 0; i < normals.Length; i++)
        //        normals[i] = -normals[i];
        //    mesh.normals = normals;


        for (int i = 0; i < reverse.Length; i += 3)
        {
            int temp = reverse[i + 0];
            reverse[i + 0] = reverse[i + 1];
            reverse[i + 1] = temp;
        }


    }
}
